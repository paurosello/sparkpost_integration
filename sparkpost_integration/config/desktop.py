# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Sparkpost Integration",
			"color": "#fa6423",
			"icon": "octicon octicon-mail",
			"type": "module",
			"label": _("Sparkpost Integration")
		}
	]
