# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from sparkpost_integration.utils import getSparkClient
from slugify import slugify

class SparkpostTemplate(Document):
	def before_save(self):
		sp = getSparkClient()
		sp.templates.update(
			self.name,
			name = self.template_name,
			description=self.description,
			subject=self.subject,
			reply_to = self.reply_to,
			text = self.text,
			html = self.html,
			track_opens = self.track_opens==1,
			track_clicks = self.track_clicks==1,
			from_email=self.from_email,
			published=self.published==1,
			is_transactional=self.is_transactional==1
		)

	def before_insert(self):
		self.id=slugify(self.template_name)
		sp = getSparkClient()
		sp.templates.create(
			id=self.id,
			name=self.template_name,
			description=self.description,
			subject=self.subject,
			reply_to=self.reply_to,
			text=self.text,
			html=self.html,
			track_opens=self.track_opens == 1,
			track_clicks=self.track_clicks == 1,
			from_email=self.from_email,
			published=self.published == 1,
			is_transactional=self.is_transactional == 1
		)
		if(self.published==1):
			sp.templates.update(
				self.id,
				name=self.template_name,
				description=self.description,
				subject=self.subject,
				reply_to=self.reply_to,
				text=self.text,
				html=self.html,
				track_opens=self.track_opens == 1,
				track_clicks=self.track_clicks == 1,
				from_email=self.from_email,
				published=self.published == 1,
				is_transactional=self.is_transactional == 1
			)

	def on_trash(self):
		sp = getSparkClient()
		sp.templates.delete(self.name)
