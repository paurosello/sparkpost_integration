/**
 * Created by pau on 29/12/2016.
 */
frappe.listview_settings['Sparkpost Template'] = {
    refresh: function (listview) {
        listview.page.clear_actions_menu();
        listview.page.add_action_item(__("Synchronize Templates"), function() {
            frappe.call({
                method:"sparkpost_integration.utils.update_templates",
                callback:function(r){
                    console.log("Templates Updated")
                }
            });
        })
    }
};