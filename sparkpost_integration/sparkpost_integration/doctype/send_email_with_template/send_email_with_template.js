// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('Send Email with Template', {
	refresh: function(frm) {
		frm.page.set_primary_action(__("Send Email"), function() {
			var me = this;
			frappe.call({
				method:"sparkpost_integration.utils.send_email_template",
				args: {
					"recipients": cur_frm.doc.recipients,
					"template": cur_frm.doc.template,
					"variables": cur_frm.doc.variables.map(function (item) {
						return {
							attribute: item.attribute,
							value: item.value
						}
					})
				},
				callback:function(r){
					console.log("ok")
				}
			});
		});
	},
	"template": function(frm){
		this.frm = frm;
		if(cur_frm.doc.template!="") {
			frappe.call({
				"method": "frappe.client.get",
				args: {
					doctype: "Sparkpost Template",
					name: cur_frm.doc.template,
					fields: ["html"]
				},
				callback: function (data) {
					var rx = /\{\{\s?\w+\s?\}\}/g;
					frm.set_value("variables",[])
					do {
						m = rx.exec(data.message.html);
						if (m) {
							var new_row = frm.add_child("variables");
							new_row.attribute = m[0].replace(/\"/g, "").replace(/\{/g, "").replace(/\}/g, "");
						}
					} while (m);

					refresh_field("variables")
				}
			})
		}
		else{
			cur_frm.set_value("variables",[])
		}
	}
});
