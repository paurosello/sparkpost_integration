import frappe
from frappe import _
from sparkpost import SparkPost
import ast
import json

def getSparkClient():
    api_key = frappe.db.get_singles_dict("Sparkpost Configuration").api_key
    if api_key!=None:
        return SparkPost(api_key)
    else:
        frappe.throw(_("You need to configure SparkPost API key"))

@frappe.whitelist()
def send_email_template(recipients, template, variables):
    sp = getSparkClient()
    variables = ast.literal_eval(variables)
    substitution_data={}
    for var in variables:
        substitution_data[var["attribute"]] = var["value"]

    response = sp.transmissions.send(
        options={
            "click_tracking": False
        },
        recipients=recipients.split(","),
        template=template,
        substitution_data = substitution_data
    )

    return response

@frappe.whitelist(allow_guest=True)
def unsubscribe():
    frappe.logger().debug("Got unsubscribe hook")
    return "ok"

@frappe.whitelist()
def update_templates():
    sp = getSparkClient()
    for template_element in sp.templates.list():
        template=sp.templates.get(template_element["id"])

        if frappe.db.exists("Sparkpost Template", template["id"]):
            doc = frappe.get_doc("Sparkpost Template", template["id"])
            doc.name =  template["id"]
            doc.template_name = template["name"]
            doc.description = template["description"]
            doc.published = template["published"]
            doc.track_opens = template["options"]["open_tracking"]
            doc.track_clicks = template["options"]["click_tracking"]
            doc.is_transactional = template["options"]["transactional"]
            doc.html = template["content"]["html"]
            doc.text = template["content"]["text"]
            doc.subject = template["content"]["subject"]
            doc.from_email = template["content"]["from"]
            doc.reply_to = template["content"]["reply_to"]
            doc.save()
        else:
            doc = frappe.new_doc("Sparkpost Template")
            doc.id =  template["id"]
            doc.template_name = template["name"]
            doc.description = template["description"]
            doc.published = template["published"]
            doc.track_opens = template["options"]["open_tracking"]
            doc.track_clicks = template["options"]["click_tracking"]
            doc.is_transactional = template["options"]["transactional"]
            doc.html = template["content"]["html"]
            doc.text = template["content"]["text"]
            doc.subject = template["content"]["subject"]
            if(isinstance(template["content"]["from"], unicode)):
                doc.from_email = template["content"]["from"]
            else:
                doc.from_email = template["content"]["from"]["email"]
            doc.reply_to = template["content"]["reply_to"]
            doc.db_insert()
    frappe.msgprint(_("Synchronization Finished"))
    frappe.publish_realtime("list_update", {"doctype": "Sparkpost Template"}, after_commit=True)
